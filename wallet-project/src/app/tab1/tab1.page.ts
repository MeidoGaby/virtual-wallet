import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Account } from '../common/Account';
import { AccountService } from '../common/AccountService';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  accounts: Account[];

  constructor(private service: AccountService) {

  }
  async ngOnInit() {
    try {
      this.accounts = await this.service.myAccounts();
      console.log(this.accounts);
    }
    catch (err) {
      console.log(err);
    }

  }

}
