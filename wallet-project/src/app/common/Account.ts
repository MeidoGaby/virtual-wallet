import { throwError } from "rxjs";

export class Account {
    id: string;
    name: string;
    accountNumber: string;
    balance: number;


    withdraw(amount: number){
        if(amount<=0) {
            throwError('Cantidad al retirar debe ser un numero mayor que 0');
        }

        if(amount>this.balance){
            throwError('Fondos insuficientes para realizar retiro');
        }
        this.balance = this.balance - amount;
        
    }

    deposit(amount: number){

        if(amount<=0) {
            throwError('Cantidad a depositar debe ser un numero mayor que 0');
        }
        this.balance = this.balance + amount;
    }

    accountNameToDisplay(){
      return this.name.toUpperCase();  
    }

    balanceToDisplay(){
        return 'Saldo Disponible: $' + this.balance.toFixed(2);
    }
}