import { Account } from "./Account";
import { getUniqueId } from "./utils";

export class HistoryEntry {
    id: string;
    originAccount: string;
    destinyAccount: string;
    date: Date;
    amount: number;

    static createTransferEntry(
        originAccount: Account,
        destinyAccount: Account,
        amount: number
    ): HistoryEntry {
        
        let historyEntry = new HistoryEntry();
        historyEntry.id = getUniqueId();
        historyEntry.originAccount = originAccount.name;
        historyEntry.destinyAccount = destinyAccount.name;
        historyEntry.amount = amount;
        historyEntry.date = new Date();
        
        return historyEntry;
    }

    amountToDisplay(){
        return 'Monto: $' + this.amount.toFixed(2);
    }
}