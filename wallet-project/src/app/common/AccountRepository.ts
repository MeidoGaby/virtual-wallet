import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Account } from './Account';
import {take, map} from 'rxjs/operators';

@Injectable({ providedIn: 'root' })

export class AccountRepositoryService {
    constructor(private db: AngularFireDatabase) { }

    public updateAccount(account: Account): Promise<void> {
       let result = this.db.object('/Account/' + account.id);
       result.update({balance: account.balance});
        return Promise.resolve();
    }

    public list(): Promise<Account[]> {

        return this.db.list<Account>('/Account').valueChanges().pipe(
            map(value => {
                let list = [];
                value.forEach(element => {
                    let account = new Account();
                    account.id = element.id;
                    account.name = element.name;
                    account.accountNumber = element.accountNumber;
                    account.balance = element.balance;
                    list.push(account);

                })
                return list;
            }),
            take(1)
        ).toPromise();
    }
}