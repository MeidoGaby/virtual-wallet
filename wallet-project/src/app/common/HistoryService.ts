import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { HistoryEntry } from './HistoryEntry';
import {take, map} from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class HistoryService {
    constructor(private db: AngularFireDatabase) { }

    create(model: HistoryEntry): Promise<void> {
        this.db.list('/History').push({
            id: model.id,
            originAccount: model.originAccount,
            destinyAccount: model.destinyAccount,
            date: model.date.getDate() + '/' + (model.date.getMonth() + 1 ) + '/' + model.date.getFullYear(),
            amount: model.amount

        })
        return Promise.resolve();
    }

    list(): Promise<HistoryEntry[]> {
        return this.db.list<HistoryEntry>('/History').valueChanges().pipe(
            map(value => {
                let list = [];
                value.forEach(element => {
                    let entry = new HistoryEntry();
                    entry.id = element.id
                    entry.originAccount = element.originAccount;
                    entry.destinyAccount = element.destinyAccount;
                    entry.amount = element.amount;
                    entry.date = element.date;
                    list.push(entry);

                })
                return list;
            }),
            take(1)
        ).toPromise();


    }
}