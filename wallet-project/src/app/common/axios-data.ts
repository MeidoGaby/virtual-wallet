import axios from "axios";

const instance = axios.create(
    {
        baseURL: 'https://wallet-a4e71-default-rtdb.firebaseio.com/'
    }
);

export default instance;