import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import { Account } from "./Account";
import { AccountRepositoryService } from "./AccountRepository";
import { HistoryEntry } from "./HistoryEntry";
import { HistoryService } from "./HistoryService";


@Injectable({
    providedIn: 'root'
  })

export class AccountService{

  constructor(private repository: AccountRepositoryService, private historyService: HistoryService){

  }

  async transferMoney(originAccount: Account, destinyAccount: Account, amount: number):Promise<void>{
    
    if(!originAccount){
      throw 'Cuenta invalida';
    }

    if(!destinyAccount) {
      throw 'Cuenta invalida';
    }

    if(!amount){
      throw 'Cantidad invalida';
    }
    
    originAccount.withdraw(amount);
    destinyAccount.deposit(amount);
    await this.repository.updateAccount(originAccount);
    await this.repository.updateAccount(destinyAccount);

    await this.historyService.create(
      HistoryEntry.createTransferEntry(
        originAccount,
        destinyAccount,
        amount
      )
    );
    return Promise.resolve();
  }

  myAccounts(): Promise<Account[]>{
    return this.repository.list();
  }

  
}