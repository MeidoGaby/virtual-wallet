import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { ToastController } from '@ionic/angular';
import { Account } from '../common/Account';
import { AccountService } from '../common/AccountService';
import instance from '../common/axios-data';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
  accounts: Account[];
  originAccount;
  destinyAccount;
  amount;


  constructor(public toastController: ToastController, private service: AccountService) {

  }
  async ngOnInit() {
    this.accounts = await this.service.myAccounts();
  }

  async failToast() {
    const toast = await this.toastController.create({
      message: 'Transferencia fallida',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  async successToast() {
    const toast = await this.toastController.create({
      message: 'Transferencia exitosa',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  destinationAccounts(): Account[] {
    if (!this.originAccount) {
      return this.accounts;
    }

    let accounts = this.accounts.slice();
    accounts.splice(accounts.indexOf(this.originAccount), 1)
    return accounts;
  }

  async transferMoney() {

    try {
      await this.service.transferMoney(this.originAccount, this.destinyAccount, this.amount);
      this.successToast();
    }
    catch {
      this.failToast();
    }

  }
}
