import { Component, OnInit } from '@angular/core';
import { HistoryEntry } from '../common/HistoryEntry';
import { HistoryService } from '../common/HistoryService';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
  entries: HistoryEntry[];

  constructor(private service: HistoryService) {

  }
  async ngOnInit() {
    try {
      this.entries = await this.service.list();
      console.log(this.entries);
    }catch (err){
      console.log(err);
    }
    
    
  }

}
