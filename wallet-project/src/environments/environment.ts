// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDaelIdEcsmlW4uMTvkAQtJEvXi2f4PVuo",
    authDomain: "wallet-a4e71.firebaseapp.com",
    projectId: "wallet-a4e71",
    storageBucket: "wallet-a4e71.appspot.com",
    messagingSenderId: "198301294913",
    appId: "1:198301294913:web:e514c3bb315dc6af00c1c0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
